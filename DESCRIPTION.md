## About

The Alertmanager handles alerts sent by client applications such as the Prometheus server. 
It takes care of deduplicating, grouping, and routing them to the correct receiver 
integration such as email, PagerDuty, or OpsGenie. 
It also takes care of silencing and inhibition of alerts.

## Features

* Grouping categorizes alerts of similar nature into a single notification.
* Inhibition is a concept of suppressing notifications for certain alerts if certain other alerts are already firing.
* Silences are a straightforward way to simply mute alerts for a given time.
* Alertmanager supports configuration to create a cluster for high availability.
